import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { APP_SETTINGS } from '../app/app.settings';

@Injectable()
export class CategoryService {

  constructor(private httpClient: HttpClientService) {}

  get(id: string): Promise<any> {
    return this.httpClient.get(APP_SETTINGS.URL.CATEGORY + '/' + id)
      .then((response) => {
        return response;
      });
  }

  getAll(): Promise<any> {
    return this.httpClient.get(APP_SETTINGS.URL.CATEGORY)
      .then((response) => {
        return response;
      });
  }

}
