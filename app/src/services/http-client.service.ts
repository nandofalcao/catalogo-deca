import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { AuthService } from './auth.service';
import { APP_SETTINGS } from './../app/app.settings';

@Injectable()
export class HttpClientService {
  private urlApi = APP_SETTINGS.URL.API;
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(
    private http: Http,
    private authService: AuthService,
  ) { }

  get(url: string): Promise<any> {
    return this.http.get(this.urlApi + url, this.options)
      .toPromise()
      .then((res: Response) => {
        return res.json() || null;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any): Promise<any> {
    let errMsg = {
      code: null,
      message: null,
      body: null
    };

    if (error instanceof Response) {
      const body = error.json() || {};
      const err = body.error || body;
      errMsg.code = error.status;
      errMsg.message = error.statusText || '';
      errMsg.body = err;
    } else {
      errMsg.message = error.message ? error.message : error;
    }

    console.error(errMsg);
    return Promise.reject(errMsg);
  }

  // private createRequestOptions() {
  //   let token = JSON.parse(this.authService.token);
  //   let headerToken = 'bearer ' + token.access_token;
  //   return new RequestOptions({ headers: new Headers({ 'Authorization': headerToken }) });
  // }

  // get(url) {
  //   return this.http.get(url, this.createRequestOptions())
  //     .catch((error: any) => {
  //       if (error.status === 401) {
  //         console.log("sem acesso");
  //         this.authService.logout();
  //         return Observable.throw('Acesso Expirou!');;
  //       }

  //       return Observable.throw(error);
  //     });
  // }

  // post(url, data) {
  //   return this.http.post(url, data, this.createRequestOptions())
  //     .catch((error: any) => {
  //       if (error.status === 401) {
  //         console.log("sem acesso");
  //         this.authService.logout();
  //         return Observable.throw('Acesso Expirou!');;
  //       }

  //       return Observable.throw(error);
  //     });
  // }

  // put(url, data) {
  //   return this.http.put(url, data, this.createRequestOptions())
  //     .catch((error: any) => {
  //       if (error.status === 401) {
  //         console.log("sem acesso");
  //         this.authService.logout();
  //         return Observable.throw('Acesso Expirou!');;
  //       }

  //       return Observable.throw(error);
  //     });;
  // }
}
