import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { APP_SETTINGS } from '../app/app.settings';

@Injectable()
export class DashboardService {

  constructor(private httpClient: HttpClientService) {}

  getAll(): Promise<any> {
    return this.httpClient.get(APP_SETTINGS.URL.DASHBOARD)
      .then((response) => {
        return response;
      });
  }

}
