import { AuthLoginModel } from './../models/auth-login.model';
import { AuthSignupModel } from './../models/auth-signup.model';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { APP_SETTINGS } from '../app/app.settings';

@Injectable()
export class AuthService {
  isLogged: boolean;
  token: string;

  constructor(private http: Http) {
    this.isLogged = false;
    this.token = JSON.parse(localStorage.getItem(APP_SETTINGS.LS.USER_TOKEN));

    if (this.token)
      this.isLogged = true;
  }

  /**
   * User Login
   * @param {AuthLoginModel} userInfoLogin
   * @returns {Promise<any>}
   */

  login(userInfoLogin: AuthLoginModel): Promise<any> {
    this.isLogged = false;
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });
    let body = 'username=' + userInfoLogin.email + '&password=' + userInfoLogin.password + '&grant_type=password&client_id=app&client_secret=app@deca';
    let url = APP_SETTINGS.URL.LOGIN;

    return this.http.post(url, body, options)
      .toPromise()
      .then((res: Response) => {
        let data = res.json();
        localStorage.setItem(APP_SETTINGS.LS.USER_TOKEN,JSON.stringify(data));
        this.token = data;
        return data;
      })
      .catch(this.handleError);
  }

  /**
   * User Signup
   * @param {AuthSignupModel} userInfoSignup
   * @returns {Promise<any>}
   */

  signup(userInfoSignup: AuthSignupModel): Promise<any> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });
    let url = APP_SETTINGS.URL.SIGNUP;
    return this.http.post(url, JSON.stringify(userInfoSignup), options)
      .toPromise()
      .then((res: Response) => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  /**
   * User Logout
   * @memberOf AuthService
   */

  logout() {
    localStorage.removeItem(APP_SETTINGS.LS.USER_TOKEN);
  }

  /**
   * Handle Error
   * @private
   * @param {(Response | any)} error
   * @returns {Promise<any>}
   */

  private handleError(error: Response | any): Promise<any> {
    let errMsg = {
      code: null,
      message: null,
      body: null
    };

    if (error instanceof Response) {
      const body = error.json() || {};
      const err = body.error || body;
      errMsg.code = error.status;
      errMsg.message = error.statusText || '';
      errMsg.body = err;
    } else {
      errMsg.message = error.message ? error.message : error;
    }

    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}
