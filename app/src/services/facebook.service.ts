import { AuthLoginModel } from './../models/auth-login.model';
import { AuthSignupModel } from './../models/auth-signup.model';
import { Injectable } from '@angular/core';
import { Facebook } from 'ionic-native';
import { APP_SETTINGS } from './../app/app.settings';
import { AuthService } from './auth.service';

@Injectable()
export class FacebookService {
  private permissions: string[] = ['public_profile', 'email'];

  constructor(private auth: AuthService) {
    Facebook.browserInit(APP_SETTINGS.FACEBOOK.APP_ID, 'v2.8');
  }


  /**
   * Login via Facebook
   */

  login(userSignup: AuthSignupModel, callback: Function) {
    Facebook.login(this.permissions)
      .then((response) => {
        console.log(JSON.stringify(response));
        this.graphApi(response.authResponse, userSignup, callback);
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        callback(false,error);
      });
  }

  /**
   * GraphApi
   * @private
   * @param {*} authResponse
   */

  private graphApi(authResponse: any, userSignup: AuthSignupModel,callback: Function) {
    Facebook.api('/me/?fields=email,name&access_token=' + authResponse.accessToken, this.permissions)
      .then((response) => {
        console.log('graphApi', JSON.stringify(response));
        let userSignupParsed: AuthSignupModel = {
          email: response.email,
          name: response.name,
          password: response.id,
          accessProfile: userSignup.accessProfile,
          facebook: {
            email: response.email,
            photo: null,
            userId: response.id,
            authResponse: authResponse
          }
        };
        return userSignupParsed
      })
      .then((userSignup: AuthSignupModel) => {
        this.userSignup(userSignup,callback);
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        callback(false,error);
      });
  }

  /**
   * User Signup
   * @private
   * @param {AuthSignupModel} userSignup
   */

  private userSignup(userSignup: AuthSignupModel, callback: Function) {
    let userLogin: AuthLoginModel = {
      email: userSignup.email,
      password: userSignup.facebook.userId
    }

    this.auth.signup(userSignup)
      .then((response) => {
        console.log('userSignup success');
        this.userLogin(userLogin,callback);
      })
      .catch((error) => {
        console.log('userSignup Error');
        this.userLogin(userLogin,callback);
      })
  }

  /**
   * User Login
   * @private
   * @param {AuthLoginInterface} userLogin
   */

  private userLogin(userLogin: AuthLoginModel, callback: Function) {
    this.auth.login(userLogin)
      .then((response) => {
        console.log('userLogin success');
        callback(true);
      })
      .catch((error) => {
        console.log(JSON.stringify(error))
        callback(false,error);
      });
  }

}
