import { Injectable } from '@angular/core';

import { APP_SETTINGS } from '../app/app.settings';

@Injectable()
export class WelcomeService {

  constructor() {}

  skipWelcomePage(): Promise<boolean> {
    const promise = new Promise((resolve, reject) => {
      localStorage.setItem(APP_SETTINGS.LS.USER_SKIP_WELCOME, JSON.stringify(true));
      resolve(true)
    });

    return promise;
  }

  resetSkipWelcomePage(): Promise<boolean> {
    const promise = new Promise((resolve, reject) => {
      localStorage.removeItem(APP_SETTINGS.LS.USER_SKIP_WELCOME);
      resolve(true)
    });

    return promise;
  }

  userSkippedWelcomePage(): Promise<boolean> {
    const promise = new Promise((resolve, reject) => {
      const value = JSON.parse(localStorage.getItem(APP_SETTINGS.LS.USER_SKIP_WELCOME));
      if (value) resolve(true);
      if (!value) reject(false);
    });
    return promise;
  }

}
