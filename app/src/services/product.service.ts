import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { APP_SETTINGS } from '../app/app.settings';

@Injectable()
export class ProductService {

  constructor(private httpClient: HttpClientService) {}

  getAll(idCategory: string): Promise<any> {
    return this.httpClient.get(APP_SETTINGS.URL.PRODUCT + '/' + idCategory)
      .then((response) => {
        return response;
      });
  }

}
