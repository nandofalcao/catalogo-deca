import { CategoryService } from './../../services/category.service';
import { ListProductPage } from './../listProduct/listProduct';
import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})

export class CategoryPage {
  private enviroment: any;
  categories: [any];

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private categorySrv: CategoryService) {
      this.enviroment = this.navParams.get('data');
  }

  ionViewDidEnter() {
    this.categorySrv.get(this.enviroment.refId)
      .then(list => {
        this.categories = list || [];
        console.log(list);
      })
      .catch(error => null);
  }

  goTo(categoryObj: any) {
    this.navCtrl.push(ListProductPage, {data: categoryObj});
  }

}
