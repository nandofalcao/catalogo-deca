import { InspirationPage } from './../inspiration/inspiration';

import { Component } from '@angular/core';

import { DashboardPage } from '../dashboard/dashboard';
import { FavoritePage } from '../favorite/favorite';
import { ProjectPage } from '../project/project';
import { ProfilePage } from '../profile/profile';
import { APP_SETTINGS } from '../../app/app.settings';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {
  // Constant Tab Settings
  TAB_SETTINGS: any = APP_SETTINGS.TABS;

  // Tabs root
  homePageTab: any = DashboardPage;
  projectPageTab: any = ProjectPage;
  favoritePageTab: any = FavoritePage;
  profilePageTab: any = ProfilePage;
  inspirationPageTab: any = InspirationPage;

  constructor() {}

}
