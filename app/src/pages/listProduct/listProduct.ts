import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductService } from './../../services/product.service';
import { ProductPage } from './../product/product';

@Component({
  selector: 'page-category',
  templateUrl: 'listProduct.html'
})

export class ListProductPage {
  private category: any;
  products: [any];
  catSelect1: string = 'banheiro';
  catSelect2: string = 'chuveiro';
  catSelect3: string = 'cozinha';
  catSelect4: string = 'usopublico';

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private productSrv: ProductService) {
      this.category = this.navParams.get('data');
  }

  ionViewDidEnter() {
    this.productSrv.getAll(this.category._id)
      .then(list => {
        this.products = list || [];
        console.log(list);
      })
      .catch(error => null);
  }

  goTo(productObj: any) {
    this.navCtrl.push(ProductPage, {data: productObj});
  }

}
