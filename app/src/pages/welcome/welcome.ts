import { AuthLoginModel } from './../../models/auth-login.model';
import { Component, ViewChild } from '@angular/core';
import { Slides, NavController, LoadingController, ToastController } from 'ionic-angular';
import { OnInit } from '@angular/core';

import { TabsPage } from './../tabs/tabs';
import { WelcomeService } from './../../services/welcome.service';
import { AuthService } from './../../services/auth.service';
import { FacebookService } from './../../services/facebook.service';
import { AuthSignupModel } from './../../models/auth-signup.model';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})

export class WelcomePage implements OnInit {
  SLIDE = {
    START: 0,
    SELECT_PROFILE: 1,
    EMAIL_FACEBOOK: 2,
    PASSWORD: 3,
    NAME: 4,
    END: 5
  }
  @ViewChild(Slides) slides: Slides;

  userSignup: AuthSignupModel = {
    email: null,
    name: null,
    password: null,
    accessProfile: null
  };

  constructor(
    private navCtrl: NavController,
    private welcomeSrv: WelcomeService,
    private facebookSrv: FacebookService,
    private auth: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {

  }

  ngOnInit() {
    // this.slides.lockSwipes(true);
  }

  slideChanged() {
    this.slides.lockSwipes(true);
    this.slides.lockSwipeToPrev(false);
  }

  /**
   * Next Slide
   */

  nextSlide() {
    switch (this.slides.getActiveIndex()) {
      case this.SLIDE.NAME:
        this.signupEmail(this.userSignup);
      default:
        this.slides.slideTo(this.slides.getActiveIndex() + 1);
    }

  }

  /**
   * Previous Slide
   */

  prevSlide() {
    this.slides.slideTo(this.slides.getActiveIndex() - 1);
  }

  /**
   * Skip Welcome
   */

  skipWelcome() {
    this.welcomeSrv.skipWelcomePage().then(() => this.navCtrl.push(TabsPage));
  }


  /**
   * Validate if user go to next slide
   * @returns {boolean}
   */
  validateNext(): boolean {
    switch (this.slides.getActiveIndex()) {
      case this.SLIDE.SELECT_PROFILE:
        if (this.userSignup.accessProfile) {
          this.slides.lockSwipeToNext(false);
          return true;
        }
        return false;

      case this.SLIDE.EMAIL_FACEBOOK:
        if (this.userSignup.email) {
          this.slides.lockSwipeToNext(false);
          return true;
        }
        return false;

      case this.SLIDE.PASSWORD:
        if (this.userSignup.password) {
          this.slides.lockSwipeToNext(false);
          return true;
        }
        return false;

      case this.SLIDE.NAME:
        if (this.userSignup.name) {
          this.slides.lockSwipeToNext(true); // click on button to signup
          return true;
        }
        return false;

      case this.SLIDE.START:
      case this.SLIDE.END:
      default:
        this.slides.lockSwipeToNext(false);
        return true;
    }
  }

  /**
   * Select profile type: professional or consumer
   * @param {boolean} value
   */
  selectAccessProfile(value: number) {
    this.userSignup.accessProfile = value;
    console.log(this.userSignup.accessProfile);
  }

  /**
   * Signup Via Facebook
   */

  signupFacebook(userSignup: AuthSignupModel) {
    let loading = this.loadingCtrl.create();
    loading.present();

    this.facebookSrv.login(this.userSignup, (sucess: boolean, error: any) => {
      let toast = this.toastCtrl.create({
        message: sucess ? 'Login efetuado com sucesso' : JSON.stringify(error),
        duration: 3000,
        position: 'bottom'
      });

      console.log('callbackFacebookLogin', sucess);

      if (sucess) {
        this.slides.lockSwipeToNext(false);
        this.slides.slideTo(this.SLIDE.END);
      }


      if (error)
        console.error(error);

      loading.dismiss();
      toast.present();
    });
  }

  /**
   * Signup Via Email
   * @param {AuthSignupModel} userSignup
   */
  signupEmail(userSignup: AuthSignupModel) {
    console.log(userSignup);
    let loading = this.loadingCtrl.create();
    loading.present();

    this.auth.signup(userSignup)
      .then((response) => {
        console.log('userSignup success');
        loading.dismiss();
        this.login(userSignup);
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        loading.dismiss();
        if (error.code === 409) {
          this.login(userSignup);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Problema no servidor ou sem conexão.',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        }
      });
  }

  /**
   * Login
   * @param {AuthSignupModel} userSignup
   */
  login(userSignup: AuthSignupModel) {
    let userLogin: AuthLoginModel = {
      email: userSignup.email,
      password: userSignup.password
    }

    let loading = this.loadingCtrl.create();
    loading.present();

    this.auth.login(userLogin)
      .then((response) => {
        console.log('userLogin success');
        let toast = this.toastCtrl.create({
          message: 'Login efetuado com sucesso',
          duration: 3000,
          position: 'bottom'
        });
        this.slides.lockSwipeToNext(false);
        this.slides.slideTo(this.SLIDE.END);

        loading.dismiss();
        toast.present();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Erro ao efetuar login',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      });
  }
}
