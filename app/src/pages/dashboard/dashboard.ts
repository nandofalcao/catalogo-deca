import { DashboardService } from './../../services/dashboard.service';
import { ListProductPage } from './../listProduct/listProduct';
import { InspirationPage } from './../inspiration/inspiration';
import { CategoryPage } from './../category/category';
import { NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})

export class DashboardPage {
  private pages: [any] = [
    { name: 'inspiration', page: InspirationPage }
  ];
  items: [any];

  constructor(
    public navCtrl: NavController,
    private dashboardSrv: DashboardService) {}

  ionViewDidEnter() {
    this.dashboardSrv.getAll()
      .then(list => {
        this.items = list || [];
        console.log(list);
      })
      .catch(error => null);
  }

  goTo(data: any) {
    switch(data.type) {
      case 'environment':
        this.navCtrl.push(CategoryPage, {data: data});
        break;

      case 'category':
        this.navCtrl.push(ListProductPage, {data: data});
        break;

      case 'page':
        this.pages.map((value) => {
          if (value.name == data.url)
            this.navCtrl.push(value.page);
        });
        break;

      default:
    }

  }

}
