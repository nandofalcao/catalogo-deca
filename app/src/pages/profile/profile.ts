import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { WelcomePage } from './../welcome/welcome';
import { WelcomeService } from './../../services/welcome.service';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})

export class ProfilePage {

  constructor(private app: App, public navCtrl: NavController, private welcomeSrv: WelcomeService) {

  }

  goWelcomePage() {
    this.welcomeSrv.resetSkipWelcomePage().then(() => this.app.getRootNav().setRoot(WelcomePage));
  }

}
