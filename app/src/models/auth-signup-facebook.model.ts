export class AuthSignupFacebookModel {
  userId: string;
  email: string;
  photo: string;
  authResponse: any;
}
