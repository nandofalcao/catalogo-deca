import { AuthSignupFacebookModel } from './auth-signup-facebook.model';

export class AuthSignupModel {
  name: string;
  email: string;
  password: string;
  accessProfile: number;
  facebook?: AuthSignupFacebookModel
}
