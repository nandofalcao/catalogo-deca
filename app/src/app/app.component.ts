import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen, Device } from 'ionic-native';
import { Component } from '@angular/core';
import { APP_SETTINGS } from './../app/app.settings';
import { TabsPage } from './../pages/tabs/tabs';
import { WelcomePage } from './../pages/welcome/welcome';
import { WelcomeService } from './../services/welcome.service';

// Declare cordova plugins
declare var hockeyapp: any;

@Component({
  templateUrl: 'app.html'
})

export class AppComponent {
  tabsPage: any;

  constructor(
    private welcomeSrv: WelcomeService,
    private platform: Platform) {
    // Platform Ready
    platform.ready().then(() => {

      StatusBar.styleDefault();
      Splashscreen.hide();

      if (this.platform.is('cordova')) {
        // HockeyApp
        if (hockeyapp !== undefined) {
          // Android
          if (Device.platform.toLowerCase() == 'android') {
            hockeyapp.start(null, null, APP_SETTINGS.HOCKEY_APP_ID.ANDROID);
            console.log("Platform is Android and set HockeyApp");
          }

          // iOS
          if (Device.platform.toLowerCase() == 'ios') {
            hockeyapp.start(null, null, APP_SETTINGS.HOCKEY_APP_ID.IOS);
            console.log("Platform is Ios and set HockeyApp");
          }
        }
      }
    });

    // Validate if user skipped welcome page
    this.welcomeSrv.userSkippedWelcomePage()
      .then(() => {
        this.tabsPage = TabsPage;
      }).catch(() => {
        this.tabsPage = WelcomePage;
      });

  }
}
