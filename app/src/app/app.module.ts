import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AppComponent } from './app.component';
import { WelcomePage } from '../pages/welcome/welcome';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { CategoryPage } from '../pages/category/category';
import { ListProductPage } from './../pages/listProduct/listProduct';
import { ProjectPage } from '../pages/project/project';
import { FavoritePage } from '../pages/favorite/favorite';
import { ProfilePage } from '../pages/profile/profile';
import { TabsPage } from '../pages/tabs/tabs';
import { AuthService } from './../services/auth.service';
import { WelcomeService } from './../services/welcome.service';
import { FacebookService } from './../services/facebook.service';
import { HttpClientService } from './../services/http-client.service';
import { ProductPage } from './../pages/product/product';
import { InspirationPage } from './../pages/inspiration/inspiration';
import { DashboardService } from './../services/dashboard.service';
import { ProductService } from './../services/product.service';
import { CategoryService } from './../services/category.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomePage,
    DashboardPage,
    CategoryPage,
    ListProductPage,
    ProjectPage,
    FavoritePage,
    ProfilePage,
    InspirationPage,
    ProductPage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(AppComponent, {
      scrollAssist: true,
      autoFocusAssist: false
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AppComponent,
    WelcomePage,
    DashboardPage,
    CategoryPage,
    ListProductPage,
    ProjectPage,
    FavoritePage,
    ProfilePage,
    InspirationPage,
    ProductPage,
    TabsPage
  ],
  providers: [
    Storage,
    WelcomeService,
    AuthService,
    HttpClientService,
    FacebookService,
    CategoryService,
    DashboardService,
    ProductService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})

export class AppModule { }
