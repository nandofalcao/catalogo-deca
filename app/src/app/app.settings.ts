export const APP_SETTINGS = {
  NAME: 'Deca Catálogo',
  ENV: 'development',
  URL: {
    API: 'http://52.67.64.132:3000/api',
    LOGIN: 'http://52.67.64.132:3000/oauth/token',
    SIGNUP: 'http://52.67.64.132:3000/api/signup',
    FORGOT_PASSWORD: 'http://52.67.64.132:3000/api/resetPassword',
    CATEGORY: '/category',
    DASHBOARD: '/dashboard',
    PRODUCT: '/product'
  },
  DB: {
    PROPERTIES: 'properties'
  },
  LS: {
    USER_SKIP_WELCOME: 'user.skipWelcome',
    USER_TOKEN: 'user.token'
  },
  FACEBOOK: {
    APP_ID: 1346412492068528
  },
  HOCKEY_APP_ID: {
    ANDROID: '4abd5410ee46453b9a6961701544e844',
    IOS: 'f624aba5f31c4fc0ada5fddd5d2ff38f'
  },
  TABS: {
    HOME: {
      NAME: 'Início',
      ICON: 'home'
    },
    PROJECT: {
      NAME: 'Projetos',
      ICON: 'construct'
    },
    FAVORITE: {
      NAME: 'Favoritos',
      ICON: 'heart'
    },
    PROFILE: {
      NAME: 'Perfil',
      ICON: 'person'
    },
    INSPIRATION: {
      NAME: 'Inspiração',
      ICON: 'images'
    }
  }
}
